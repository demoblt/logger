package com.gutierrez.bltx.logger.util;

import java.text.DateFormat;
import java.util.Date;

import com.gutierrez.bltx.logger.dto.LoggerDto;
import com.gutierrez.bltx.logger.exception.LoggerException;

/**
 * 
 * @author cgutiron
 *
 */
public class LoggerMsgUtil {

	/**
	 * 
	 * @param messageText
	 * @param logLevel
	 * @return
	 */
	public static String formatMessage(final LoggerDto logger) {
		final String messageFormated;
		messageFormated = logger.getLevel().toString() + " " + DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) + " " + logger.getMessage().trim();
		return messageFormated;
	}

	/**
	 * 
	 * @param logger
	 * @throws LoggerException
	 * @throws Exception
	 */
	public static void validateTextMsg(final LoggerDto logger) throws LoggerException {
		String messageText = logger.getMessage();
		if (messageText == null || messageText.trim().isEmpty()) {
			throw new LoggerException("Message Text shouldnt be null or empty");
		}
	}

	/**
	 * 
	 * @param db
	 * @param file
	 * @param console
	 * @throws Exception
	 */
	public static void validateAppender(boolean db, boolean file, boolean console) throws LoggerException {
		if (!db && !file && !console) {
			throw new LoggerException("Configuration for appender of log invalid");
		}
	}
}
