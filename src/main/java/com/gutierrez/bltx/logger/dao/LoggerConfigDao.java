package com.gutierrez.bltx.logger.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.gutierrez.bltx.logger.config.db.LoggerConfigDb;
import com.gutierrez.bltx.logger.dto.DataSourceDto;
import com.gutierrez.bltx.logger.dto.LoggerDto;
import com.gutierrez.bltx.logger.exception.LoggerException;

public class LoggerConfigDao {

	private DataSourceDto	dataSourceDto;

	private boolean			state;

	public LoggerConfigDao(DataSourceDto dataSourceDto, boolean state) {
		this.dataSourceDto = dataSourceDto;
		this.state = state;
	}

	/**
	 * 
	 * @param message
	 * @param logLevel
	 * @throws LoggerException
	 */
	public int logging(final LoggerDto logger, final String message) throws LoggerException {
		int statusOpe = 0;
		if (state) {
			Connection dbConnection = LoggerConfigDb.makeDBConnection(this.dataSourceDto);
			PreparedStatement pstatementInsert = null;
			String QUERY = "INSERT INTO LOG_VALUES (MESSAGE, LEVEL) VALUES (?,?)";
			try {
				pstatementInsert = dbConnection.prepareStatement(QUERY);
				pstatementInsert.setString(1, message);
				pstatementInsert.setInt(2, logger.getLevel().getValue());
				statusOpe = pstatementInsert.executeUpdate();
			} catch (SQLException sqlException) {
				LoggerConfigDb.rollbackConnection(dbConnection);
			} finally {
				if (pstatementInsert != null)
					LoggerConfigDb.closePreparedStatement(pstatementInsert);
				if (dbConnection != null)
					LoggerConfigDb.closeConnection(dbConnection);
			}
		}
		return statusOpe;
	}

	public DataSourceDto getDataSourceDto() {
		return dataSourceDto;
	}

	public void setDataSourceDto(DataSourceDto dataSourceDto) {
		this.dataSourceDto = dataSourceDto;
	}

	public boolean getState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

}
