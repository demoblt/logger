package com.gutierrez.bltx.logger.dto;


public class DataSourceDto {

	 private String usernameDb;
	 private String passswodDb;
	 private String dbms;
	 private String serverName;
	 private String database;
	 private String portNumber;
	 
	public DataSourceDto(String usernameDb, String passswodDb, String dbms, String serverName, String database, String portNumber) {
		this.usernameDb = usernameDb;
		this.passswodDb = passswodDb;
		this.dbms = dbms;
		this.serverName = serverName;
		this.database = database;
		this.portNumber = portNumber;
	}

	
	public DataSourceDto() {
	}


	public String getUsernameDb() {
		return usernameDb;
	}

	
	public void setUsernameDb(String usernameDb) {
		this.usernameDb = usernameDb;
	}

	
	public String getPassswodDb() {
		return passswodDb;
	}

	
	public void setPassswodDb(String passswodDb) {
		this.passswodDb = passswodDb;
	}

	
	public String getDbms() {
		return dbms;
	}

	
	public void setDbms(String dbms) {
		this.dbms = dbms;
	}

	
	public String getServerName() {
		return serverName;
	}

	
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	
	public String getDatabase() {
		return database;
	}

	
	public void setDatabase(String database) {
		this.database = database;
	}

	
	public String getPortNumber() {
		return portNumber;
	}

	
	public void setPortNumber(String portNumber) {
		this.portNumber = portNumber;
	}
	 
	 
}
