package com.gutierrez.bltx.logger.dto;


public class LoggerDto {

	private String message;
	private LevelEnum level;
	
	public LoggerDto(String message, LevelEnum level) {
		this.message = message;
		this.level = level;
	}

	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	
	public LevelEnum getLevel() {
		return level;
	}

	
	public void setLevel(LevelEnum level) {
		this.level = level;
	}
}
