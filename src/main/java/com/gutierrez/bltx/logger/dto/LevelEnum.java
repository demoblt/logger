package com.gutierrez.bltx.logger.dto;


public enum LevelEnum {
	MESSAGE(1),
	ERROR(2),
	WARNING(3);
	
	private int value;
	
	private LevelEnum(int value) {
		this.value= value;
	}
	
	public int getValue() {
		return this.value;
	}
} 
