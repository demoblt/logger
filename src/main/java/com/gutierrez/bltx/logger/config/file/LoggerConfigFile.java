package com.gutierrez.bltx.logger.config.file;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.gutierrez.bltx.logger.exception.LoggerException;

/**
 * 
 * @author cgutiron
 *
 */
public class LoggerConfigFile {

	private String	logFileFolder;
	private boolean	state;
	private String	nameLogFile;

	public LoggerConfigFile(String logFileFolder, boolean state, String nameLogFile) {
		this.logFileFolder = logFileFolder;
		this.state = state;
		this.nameLogFile = nameLogFile;
	}

	/**
	 * 
	 * @throws LoggerException
	 */
	private void createFile() throws LoggerException {
		File logFile = new File(this.logFileFolder + "\\" + this.nameLogFile);
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException ex) {
				throw new LoggerException(ex);
			}
		}
	}

	/**
	 * 
	 * @param logger
	 * @param messageText
	 * @throws LoggerException
	 */
	public void logging(final Logger logger, final String messageText) throws LoggerException {
		if (this.state) {
			this.createFile();
			FileHandler fileHandler;
			try {
				fileHandler = new FileHandler(this.logFileFolder + "\\" + this.nameLogFile);
			} catch (SecurityException ex) {
				throw new LoggerException(ex);
			} catch (IOException ex) {
				throw new LoggerException(ex);
			}
			logger.addHandler(fileHandler);
			logger.log(Level.INFO, messageText);
			fileHandler.close();
		}
	}

	public String getLogFileFolder() {
		return logFileFolder;
	}

	public void setLogFileFolder(String logFileFolder) {
		this.logFileFolder = logFileFolder;
	}

	public boolean getState() {
		return this.state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public String getNamelogFile() {
		return nameLogFile;
	}

	public void setNamelogFile(String namelogFile) {
		this.nameLogFile = namelogFile;
	}
}
