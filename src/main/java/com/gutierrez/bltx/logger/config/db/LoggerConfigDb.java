package com.gutierrez.bltx.logger.config.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.gutierrez.bltx.logger.dto.DataSourceDto;
import com.gutierrez.bltx.logger.exception.LoggerException;

/**
 * 
 * @author cgutiron
 *
 */
public final class LoggerConfigDb {

	private static final String JDBC = "jdbc";

	/**
	 * 
	 * @param dataSourceDto
	 * @return
	 * @throws LoggerException 
	 */
	public static Connection makeDBConnection(DataSourceDto dataSourceDto) throws LoggerException {
		Connection dbConnection = null;
		String url = JDBC + ":" + dataSourceDto.getDbms() + "://" + dataSourceDto.getServerName() + ":" + dataSourceDto.getPortNumber() + "/"
				+ dataSourceDto.getDatabase();
		try {
			dbConnection = DriverManager.getConnection(url, dataSourceDto.getUsernameDb(), dataSourceDto.getPassswodDb());
		} catch (SQLException ex) {
			throw new LoggerException(ex);
		}

		return dbConnection;
	}

	public static void closeConnection(Connection dbConnection) throws LoggerException {
		try {
			dbConnection.close();
		} catch (SQLException ex) {
			throw new LoggerException(ex);
		}
	}

	public static void closePreparedStatement(PreparedStatement pStatement) throws LoggerException {
		try {
			pStatement.close();
		} catch (SQLException ex) {
			throw new LoggerException(ex);
		}
	}
	
	public static void rollbackConnection(Connection dbConnection) throws LoggerException {
		try {
			dbConnection.rollback();
		} catch (SQLException ex) {
			throw new LoggerException(ex);
		}
	}

}
