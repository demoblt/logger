package com.gutierrez.bltx.logger.config.console;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author cgutiron
 *
 */
public class LoggerConfigConsole {

	private boolean state;

	public LoggerConfigConsole(boolean state) {
		this.state = state;
	}

	/**
	 * 
	 * @param logger
	 * @param messageText
	 */
	public void logging(final Logger logger, final String messageText) {
		if (this.state) {
			final ConsoleHandler consoleHandler = new ConsoleHandler();
			logger.addHandler(consoleHandler);
			logger.log(Level.INFO, messageText);
		}

	}

	public boolean getState() {
		return this.state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

}
