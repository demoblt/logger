package com.gutierrez.bltx.logger.endpoint;

import java.util.logging.Logger;

import com.gutierrez.bltx.logger.config.console.LoggerConfigConsole;
import com.gutierrez.bltx.logger.config.file.LoggerConfigFile;
import com.gutierrez.bltx.logger.dao.LoggerConfigDao;
import com.gutierrez.bltx.logger.dto.LoggerDto;
import com.gutierrez.bltx.logger.exception.LoggerException;
import com.gutierrez.bltx.logger.util.LoggerMsgUtil;

/**
 * 
 * @author cgutiron
 *
 */
public class JobLogger {

	private LoggerConfigDao		loggerConfigDao;
	private LoggerConfigConsole	loggerConfigConsole;
	private LoggerConfigFile	loggerConfigFile;
	private static final Logger	logger	= Logger.getLogger(JobLogger.class.getName());

	public JobLogger(LoggerConfigDao loggerConfigDao, LoggerConfigConsole loggerConfigConsole, LoggerConfigFile loggerConfigFile) {
		this.loggerConfigDao = loggerConfigDao;
		this.loggerConfigConsole = loggerConfigConsole;
		this.loggerConfigFile = loggerConfigFile;
	}

	/**
	 * 
	 * @param loggerDto
	 * @throws LoggerException
	 */
	public void logMessage(LoggerDto loggerDto) throws LoggerException {
		this.validateLogger(loggerDto);
		String formattedTextMsg = LoggerMsgUtil.formatMessage(loggerDto);
		this.loggerConfigConsole.logging(logger, formattedTextMsg);
		this.loggerConfigFile.logging(logger, formattedTextMsg);
		this.loggerConfigDao.logging(loggerDto, formattedTextMsg);
	}

	/**
	 * 
	 * @param logger
	 * @throws LoggerException
	 */
	private void validateLogger(LoggerDto logger) throws LoggerException {
		LoggerMsgUtil.validateTextMsg(logger);
		LoggerMsgUtil.validateAppender(this.loggerConfigDao.getState(), this.loggerConfigFile.getState(), this.loggerConfigConsole.getState());
	}

}
