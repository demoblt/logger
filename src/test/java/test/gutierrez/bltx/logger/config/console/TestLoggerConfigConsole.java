package test.gutierrez.bltx.logger.config.console;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.logging.Logger;

import org.junit.Test;

import com.gutierrez.bltx.logger.config.console.LoggerConfigConsole;

public class TestLoggerConfigConsole {
	
	private static final boolean ENABLE = true; 
	private static final boolean DISABLE = false; 
	private static final Logger	LOGGER	= Logger.getLogger(TestLoggerConfigConsole.class.getName());
	private static final String MESSAGE_TEXT ="Test for demo"; 
	
	@Test
	public void testLoggingEnable() {
		LoggerConfigConsole loggerConfigConsole = new LoggerConfigConsole(ENABLE);
		loggerConfigConsole.logging(LOGGER, MESSAGE_TEXT);
		assertNotNull(LOGGER.getHandlers());
	}
	
	@Test
	public void testLoggingDisable() {
		LoggerConfigConsole loggerConfigConsole = new LoggerConfigConsole(DISABLE);
		loggerConfigConsole.logging(LOGGER, MESSAGE_TEXT);
		assertEquals(1,LOGGER.getHandlers().length);
	}
}
