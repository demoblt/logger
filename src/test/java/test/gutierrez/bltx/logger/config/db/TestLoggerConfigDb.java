package test.gutierrez.bltx.logger.config.db;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.gutierrez.bltx.logger.config.db.LoggerConfigDb;
import com.gutierrez.bltx.logger.dto.DataSourceDto;
import com.gutierrez.bltx.logger.exception.LoggerException;

public class TestLoggerConfigDb {

	private static final String	USERNAMEDB	= "root";
	private static final String	PASSSWORDDB	= "";
	private static final String	DBMS		= "mysql";
	private static final String	SERVERNAME	= "localhost";
	private static final String	DATABASE	= "test";
	private static final String	PORTNUMBER	= "3306";
	private DataSourceDto		dataSourceDto = new DataSourceDto() ;

	@InjectMocks
	private LoggerConfigDb loggerConfigDb;
	
	@Mock
	private Connection connection;
	
	@Mock
	private PreparedStatement preparedStatement;
	
	@Before
	public void setUp() {
		dataSourceDto.setDatabase(DATABASE);
		dataSourceDto.setDbms(DBMS);
		dataSourceDto.setServerName(SERVERNAME);
		dataSourceDto.setPortNumber(PORTNUMBER);
		dataSourceDto.setUsernameDb(USERNAMEDB);
		dataSourceDto.setPassswodDb(PASSSWORDDB);
	}
	
	@Test
	public void testDbConnection() throws LoggerException {
		assertNotNull(LoggerConfigDb.makeDBConnection(dataSourceDto));
	}
	
	@Test(expected = LoggerException.class)
	public void testDbConnectionException() throws LoggerException {
		dataSourceDto.setPortNumber("4422");
		LoggerConfigDb.makeDBConnection(dataSourceDto);
	}
	
	@Test
	public void testCloseConnection() throws LoggerException, SQLException {
		dataSourceDto.setPortNumber("3306");
		Connection connection = LoggerConfigDb.makeDBConnection(dataSourceDto);
		LoggerConfigDb.closeConnection(connection);
		assertTrue(connection.isClosed());
	}	
	
	
}
