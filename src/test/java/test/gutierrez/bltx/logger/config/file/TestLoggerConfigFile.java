package test.gutierrez.bltx.logger.config.file;

import static org.junit.Assert.assertNotNull;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.gutierrez.bltx.logger.config.file.LoggerConfigFile;
import com.gutierrez.bltx.logger.exception.LoggerException;

public class TestLoggerConfigFile {

	private static final String		LOGFILEFOLDER	= "C:\\";
	private static final boolean	STATE			= true;
	private static final String		NAMELOGFILE		= "logFile.txt";
	private static final String		MESSAGE_TEXT	= "Test for Test";
	private static final Logger		LOGGER			= Logger.getLogger(TestLoggerConfigFile.class.getName());
	private LoggerConfigFile		loggerConfigFile;

	@Before
	public void setUp() {
		loggerConfigFile = new LoggerConfigFile(LOGFILEFOLDER, STATE, NAMELOGFILE);
	}

	@Test
	public void testLoggin() throws LoggerException {
		loggerConfigFile.logging(LOGGER, MESSAGE_TEXT);
		assertNotNull(LOGGER.getHandlers());
	}
}
