package test.gutierrez.bltx.logger.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.gutierrez.bltx.logger.dao.LoggerConfigDao;
import com.gutierrez.bltx.logger.dto.DataSourceDto;
import com.gutierrez.bltx.logger.dto.LevelEnum;
import com.gutierrez.bltx.logger.dto.LoggerDto;
import com.gutierrez.bltx.logger.exception.LoggerException;

public class TestLoggerConfigDao {

	private static final String		USERNAMEDB		= "root";
	private static final String		PASSSWORDDB		= "";
	private static final String		DBMS			= "mysql";
	private static final String		SERVERNAME		= "localhost";
	private static final String		DATABASE		= "test";
	private static final String		PORTNUMBER		= "3306";
	private DataSourceDto			dataSourceDto	= new DataSourceDto();
	private static final boolean	STATE			= true;
	private LoggerConfigDao			loggerConfigDao;
	private static final String		MESSAGE_TEXT	= "Test for test";
	private LoggerDto				loggerDto;

	@Before
	public void setUp() {
		dataSourceDto.setDatabase(DATABASE);
		dataSourceDto.setDbms(DBMS);
		dataSourceDto.setServerName(SERVERNAME);
		dataSourceDto.setPortNumber(PORTNUMBER);
		dataSourceDto.setUsernameDb(USERNAMEDB);
		dataSourceDto.setPassswodDb(PASSSWORDDB);
	}

	@Test
	public void testLogging() throws LoggerException {
		loggerConfigDao = new LoggerConfigDao(dataSourceDto, STATE);
		loggerDto = new LoggerDto("", LevelEnum.WARNING);
		assertEquals(1,loggerConfigDao.logging(loggerDto, MESSAGE_TEXT));
	}
}
