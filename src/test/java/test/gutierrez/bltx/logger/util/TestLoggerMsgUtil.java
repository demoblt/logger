package test.gutierrez.bltx.logger.util;

import static org.junit.Assert.assertNotNull;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.gutierrez.bltx.logger.dto.LevelEnum;
import com.gutierrez.bltx.logger.dto.LoggerDto;
import com.gutierrez.bltx.logger.exception.LoggerException;
import com.gutierrez.bltx.logger.util.LoggerMsgUtil;

public class TestLoggerMsgUtil {

	private static final String	MESSAGE	= "Test for test";
	private LoggerDto			loggerDto;
	private static Logger		logger	= Logger.getLogger(TestLoggerMsgUtil.class.getName());

	@Before
	public void setUp() {
		loggerDto = new LoggerDto(MESSAGE, LevelEnum.MESSAGE);
	}

	@Test
	public void testFormatMessage() {
		String messageFormatted = LoggerMsgUtil.formatMessage(loggerDto);
		logger.info(messageFormatted);
		assertNotNull(messageFormatted);
	}

	@Test(expected = LoggerException.class)
	public void testValidateTextMsgNull() throws LoggerException {
		loggerDto.setMessage(null);
		LoggerMsgUtil.validateTextMsg(loggerDto);
	}
	
	@Test(expected = LoggerException.class)
	public void testValidateTextMsgEmpty() throws LoggerException {
		loggerDto.setMessage("");
		LoggerMsgUtil.validateTextMsg(loggerDto);
	}
	
	@Test(expected = LoggerException.class)
	public void testValidateAppender() throws LoggerException {
		try {
			LoggerMsgUtil.validateAppender(false,false,false);
		} catch (LoggerException e) {
			logger.warning(e.getMessage());
			throw new LoggerException(e);
		}
	}
}
