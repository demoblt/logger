package test.gutierrez.bltx.logger.endpoint;

import java.sql.SQLException;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.gutierrez.bltx.logger.config.console.LoggerConfigConsole;
import com.gutierrez.bltx.logger.config.file.LoggerConfigFile;
import com.gutierrez.bltx.logger.dao.LoggerConfigDao;
import com.gutierrez.bltx.logger.dto.DataSourceDto;
import com.gutierrez.bltx.logger.dto.LevelEnum;
import com.gutierrez.bltx.logger.dto.LoggerDto;
import com.gutierrez.bltx.logger.endpoint.JobLogger;
import com.gutierrez.bltx.logger.exception.LoggerException;

public class TestJobLogger {

	private static final boolean	ENABLE			= true;
	private static final Logger		LOGGER			= Logger.getLogger(TestJobLogger.class.getName());
	private static final String		MESSAGE_TEXT	= "Test for demo";
	private LoggerConfigConsole		loggerConfigConsole;

	private static final String		USERNAMEDB		= "root";
	private static final String		PASSSWORDDB		= "";
	private static final String		DBMS			= "mysql";
	private static final String		SERVERNAME		= "localhost";
	private static final String		DATABASE		= "test";
	private static final String		PORTNUMBER		= "3306";
	private DataSourceDto			dataSourceDto;
	private LoggerConfigDao			loggerConfigDao;
	private LoggerDto				loggerDto;
	
	private static final String		LOGFILEFOLDER	= "C:\\";
	private static final String		NAMELOGFILE		= "logFile.txt";
	private LoggerConfigFile		loggerConfigFile;
	
	private JobLogger jobLogger;

	@Before
	public void setUp() {
		loggerConfigConsole = new LoggerConfigConsole(ENABLE);

		dataSourceDto = new DataSourceDto();
		dataSourceDto.setDatabase(DATABASE);
		dataSourceDto.setDbms(DBMS);
		dataSourceDto.setServerName(SERVERNAME);
		dataSourceDto.setPortNumber(PORTNUMBER);
		dataSourceDto.setUsernameDb(USERNAMEDB);
		dataSourceDto.setPassswodDb(PASSSWORDDB);
		loggerConfigDao = new LoggerConfigDao(dataSourceDto, ENABLE);
		
		loggerConfigFile = new LoggerConfigFile(LOGFILEFOLDER, ENABLE, NAMELOGFILE);
		
		this.jobLogger = new JobLogger(loggerConfigDao, loggerConfigConsole, loggerConfigFile);
	}
	
	@Test
	public void testLogMessage() throws LoggerException {
		loggerDto = new LoggerDto(MESSAGE_TEXT, LevelEnum.WARNING);
		this.jobLogger.logMessage(loggerDto);
	}

}
